import '../customize/styles.less'
import Head from 'next/head'
import Link from 'next/link'
import * as React from 'react'
import { useState } from 'react'
import { AppProps } from 'next/app'
import { Col, Drawer, Icon, Layout, Menu, Row } from 'antd'
import { CeecLogo } from '../components/custom-svg'
import config from '../customize/config'

const { Header, Footer, Content } = Layout

export default function MyApp({ Component, pageProps }: AppProps): JSX.Element {

  const [ menuVisible, setMenuVisible ] = useState(false)

  const mainMenu = (mode, theme?) => {
    return (
      <Menu
        mode={mode}
        theme={theme || "light"}
      >
        <Menu.Item key="featured-tours">
          <Link href="/featured-tours" passHref>
            <a>Featured Tours</a>
          </Link>
        </Menu.Item>
        <Menu.Item key="places-of-interest">
          <Link href="/places-of-interest" passHref>
            <a>Places of Interest</a>
          </Link>
        </Menu.Item>
        <Menu.Item key="about-experiencee">
          <Link href="/about-experiencee" passHref>
            <a>About ExperienCEE</a>
          </Link>
        </Menu.Item>
      </Menu>
    )
  }

  return (
    <Layout>
      <Header>
        <Head>
          <link rel="stylesheet" href="https://www.euholidays.com.sg/assets/frontend/css/bootstrap.min.css"/>
          <link rel="stylesheet" href="https://www.euholidays.com.sg/assets/frontend/css/font-awesome.min.css"/>
          <link rel="stylesheet" href="https://www.euholidays.com.sg/assets/frontend/css/style.css"/>
          <link rel="stylesheet" href="https://www.euholidays.com.sg/assets/frontend/css/responsive.css"/>
          <link rel="stylesheet" href="https://www.euholidays.com.sg/assets/frontend/css/owl.carousel.min.css"/>
          <link rel="stylesheet" href="https://www.euholidays.com.sg/assets/frontend/css/owl.theme.green.min.css"/>
          <link rel="stylesheet" href="https://www.euholidays.com.sg/assets/frontend/css/responsive.css"/>
          <link rel="stylesheet" href="https://www.euholidays.com.sg/assets/frontend/css/custom.css"/>
          <link href="https://fonts.googleapis.com/css?family=Lato:400,700&amp;subset=latin-ext" rel="stylesheet"/>
          <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.3/css/bootstrap-select.min.css"/> 
          <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel="stylesheet"/> 
          <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/9.8.1/css/bootstrap-slider.min.css"/>
          <link rel="stylesheet" href="https://www.euholidays.com.sg/assets/lib/lightbox/css/lightgallery.css"/>
          <link rel="stylesheet" href="https://www.euholidays.com.sg/assets/frontend/css/select2.css"/>
          <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"/>
          <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossOrigin="anonymous"></script>
          <meta name="viewport" content="width=device-width, initial-scale=1.0" />
          <title>CEEC</title>
        </Head>
        <Row type="flex" gutter={20} justify="space-between" align="middle">
          <Link href="/" passHref>
            <a><CeecLogo /></a>
          </Link>
          {mainMenu("horizontal", "dark")}
          <a className="menu-trigger" onClick={() => setMenuVisible(true)}>
            <Icon type="menu" style={{ fontSize: 20 }} />
          </a>
        </Row>
        <Drawer
          visible={menuVisible}
          closable={true}
          onClose={() => setMenuVisible(false)}
          title={"Welcome"}
          width="100%"
          bodyStyle={{ padding: 0 }}
        >
          {mainMenu("vertical")}
        </Drawer>
      </Header>
      <Content>
        <Component {...pageProps} />
      </Content>
      <Footer>
        <div className="wrap pad-y">
          <Row type="flex">
            <Col xs={24} md={8} style={{ marginBottom: 24 }}>
              <CeecLogo />
              <br />
              <Row type="flex" gutter={12} align="middle" className="social">
                <a target="_blank" href={config.facebook}><Col><img src="/img/circ-icon-fb.png" height={50} /></Col></a>
                <a target="_blank" href={config.instagram}><Col><img src="/img/circ-icon-ig.png" height={50} /></Col></a>
                <a target="_blank" href={config.whatsapp}><Col><img src="/img/circ-icon-wa.png" height={50} /></Col></a>
              </Row>
            </Col>
            <Col xs={24} md={8} style={{ marginBottom: 24 }}>
              <h4>Browse</h4>
              <Link href="/featured-tours" passHref><a><div>Featured Tours</div></a></Link>
              <Link href="/places-of-interest" passHref><a><div>Places of Interest</div></a></Link>
              <Link href="/about-experiencee" passHref><a><div>About experienCEE</div></a></Link>
            </Col>
            <Col xs={24} md={8} style={{ marginBottom: 24 }}>
              <h4>Need Quick Answers?</h4>
              <Link href="/faq" passHref><a><div>FAQ</div></a></Link>
              <Link href="/chat" passHref><a><div>Chat with us</div></a></Link>
            </Col>
          </Row>
        </div>
      </Footer>
    </Layout>
  )
}
