import React, { useEffect, useState } from "react"
import { useRouter } from 'next/router'
import { Button, Icon, Input, Select, Spin, Switch, Table } from "antd";
import { totalmem } from "node:os";

const rooms = {
  roomno: 1,
  adult: 2000,
  child: 500
}

const OutBoundPage = ({ tourType } : {tourType: string}) => {
  const [rows, setRows] = useState(1)
  const [priceCheckTotal, setPriceCheckTotal] = useState(<></>)
  const [adultRoom, setAdultRoom] = useState(1)
  const [childRoom, setChildRoom] = useState(0)
  const [step, setStep] = useState(1)
  const [display1, settDisplay1] = useState('block')
  const [display2, settDisplay2] = useState('none')
  
  // Setting number of rows to based on room requirements
  function row (row) {
    setRows(row.target.value)
  }

  function setAdult(e) {
    setAdultRoom(e.target.value)
  }

  function setChild(e) {
    setChildRoom(e.target.value)
  }

  /* 
    * Computing price for individuals and cart totalmem.
    * We can also add tax and othe cost if applicable 
    * This is on click of Price chcek button. But we can also make on change of values.
  */
  const priceCheck = () => {
      setPriceCheckTotal(<div className="row bg-white">
              <div className="col-sm-12">
                <div id="checkPrices"><table className="guest-details pd10">
                    <tbody><tr className="guest-tbl-bg">
                        <th scope="col">Fare</th>
                        <th scope="col">Pax</th>
                        <th scope="col">SGD</th>
                      </tr><tr>
                        <td className="text-left-r"><strong>Adult</strong> <br />(Single - {rooms.adult})</td>
                        <td className="text-center"> x {adultRoom}</td>
                        <td className="text-right">{rooms.adult * adultRoom}</td>
                      </tr><tr>
                        <td colSpan={3}>&nbsp;</td>
                      </tr>
                      <tr>
                        <td className="text-left-r"><strong>Child</strong> <br />(Single - {rooms.child})</td>
                        <td className="text-center"> x {childRoom}</td>
                        <td className="text-right">{rooms.child * childRoom}</td>
                      </tr><tr>
                        <td colSpan={3}>&nbsp;</td>
                      </tr><tr>
                        <td colSpan className="headText text-left-r"><strong>Sub Total</strong></td>
                        <td>&nbsp;</td>
                        <td className="headText2">{rooms.adult * adultRoom + rooms.child * childRoom}</td>
                      </tr>
                      {/* <tr>
                        <td colSpan className="text-left-r"><strong>Airport Tax</strong><br />
                          Adult (645.00)</td>
                        <td>x1</td>
                        <td className="rateBox">645.00</td>
                      </tr> */}
                      <tr className="total-row">
                        <td colSpan className="headText4 text-left-r"><div className><strong>Total</strong></div></td>
                        <td />
                        <td className="headText4 total_amt text-right"><div className>{rooms.adult * adultRoom + rooms.child * childRoom}</div></td>
                      </tr><tr className="total-row">
                        <td className="headText4 text-left-r"><div className><strong>Deposit Due</strong></div></td>
                        <td />
                        <td className="headText4 total_amt text-right"><div className>1,000.00</div></td>
                      </tr><tr>
                        <td colSpan={3} className="borderlast" />
                      </tr>
                    </tbody></table><div className="row" style={{marginTop: '10px'}}>
                    <div className="col-md-4 pull-right">
                      <Button id="step1" className="btn btn-sign summarybutton" onClick={nextPage} >Next</Button>
                    </div>
                  </div></div>
              </div>
            </div>)
  }

  // Seting values to display accurate fucntionality
  function nextPage() {
    setStep(2)
    settDisplay1('none')
    settDisplay2('block')
  }

  function backtoStep1() {
    setStep(1)
    settDisplay1('block')
    settDisplay2('none')
  }

  const bookingJsx = <>
      <div className="container">
        <div className="col-md-8 col-sm-12 col-xs-12">
          <div className="row">
            <div className="col-sm-12 bg-bg">
              <span className="details">Your Details</span>
              <span className="confirm-details">Confirm Details And Payment</span>
            </div>
          </div>
          <div className="clearfix" />
          <div className="row">
            <div className="tour-title-detail">
              <span>TOUR DETAILS</span>						
              <input type="hidden" id="tour_ids" defaultValue={6889} /><input type="hidden" id="tour_codes" defaultValue="ECERPS" />					
            </div>
          </div>
          <div className="clearfix" />
          <div className="row">
            <table className="table tour-table">
              <tbody><tr>
                  <th>Tour name</th>
                  <th>Tour Type</th>
                  <th>Departure Date</th>
                  <th>Return Date</th>
                </tr>
                <tr>
                  <td>14D11N ITALY SWISS PARIS+2N DXB</td>
                  <td>05ECERPS13/21EK </td>
                  <td>
                    <input type="date" name="departureDate" className="form-control"/>								
                  </td>
                  <td>
                  <input type="date" name="returnDate" className="form-control"/>							
                  </td>
                </tr>
              </tbody></table>
          </div>
          <div className="clearfix" />
          <div className="row" style={{display: display1}}>
            <div className="col-md-1">
              <img src="https://www.euholidays.com.sg/assets/frontend/images/room-icon.png" className="room-ico" align="left" />
            </div>
            <div className="col-md-3">
              <h3 className="sub-title">Room Details</h3>
            </div>
            <div className="col-md-8">
              <hr className="customhr" />
            </div>
          </div>
          <div className="clearfix" />
          <div className="row" id="checkpricestep" style={{display: display1}}>
            <form id="checkpaxform" method="post" encType="multipart/form-data" name="checkpaxform">
              <div className="form-group">
                <label className="col-md-4 control-label">Select the No. of room</label>
                <div className="col-md-2">
                  <select className="form-control" id="no-room" style={{marginTop: '-10px'}} onChange={row}
                  >
                    <option value={1}>1</option>
                    <option value={2}>2</option>
                    <option value={3}>3</option>
                  </select>
                  <input type="hidden" defaultValue={1} id="roomNo" name="roomNo" />					 		
                </div>
              </div>
              <div className="clearfix" />
              <br />
              <div className="room-row" id="r1">
                <table className="room-tbl">
                  <tr>
                    <td className="col-sm-2 roomtitle">Room 1</td>
                    <td><span className="rtxt">Adult</span><br /><span className="age">12yrs &amp; above</span></td>
                    <td>
                      <select className="form-control adultrooms" name="adultRoom1" onChange={setAdult}>
                        <option value={1}>1</option>
                        <option value={2}>2</option>
                        <option value={3}>3</option>
                      </select>
                      <input type="hidden" name="childHfTWNRoom1" defaultValue={0} />
                    </td>
                    <td><span className="rtxt">Child</span><br /><span className="age">2 - 11yrs</span></td>
                    <td>
                      <select className="form-control selwtext" name="childWBRoom1" onChange={setChild}>
                        <option value={0}>0</option>
                        <option value={1}>1</option>
                        <option value={2}>2</option>
                      </select>
                      <span className="age">W/Bed</span>
                    </td>
                    <td>
                      <select className="form-control selwtext" name="childWOBRoom1">
                        <option value={0}>0</option>
                        <option value={1}>1</option>
                      </select>
                      <span className="age">WO/Bed</span>
                    </td>
                    <td><span className="rtxt">Infant</span><br /><span className="age">Below 2yrs</span></td>
                    <td>
                      <select className="form-control infantrooms" name="infantRoom1">
                        <option value={0}>0</option>
                        <option value={1}>1</option>
                        <option value={2}>2</option>
                      </select>
                    </td>
                    <td>
                      &nbsp;
                    </td>
                  </tr>
                  {/* <tr>
                    <td className="col-sm-2 roomtitle">Room 2</td>
                    <td><span className="rtxt">Adult</span><br/><span className="age">12yrs &amp; above</span></td>
                    <td>
                      <select className="form-control adultrooms" name="adultRoom2" id="adultRoom2">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                      </select>
                    </td>
                    <td><span class="rtxt">Child</span><br><span class="age">2 - 11yrs</span></td>
                    <td class="">
                      <select class="form-control selwtext" name="childWBRoom2" id="childWBRoom2">
                        <option value="0">0</option>
                        <option value="1">1</option>
                        <option value="1">2</option>
                      </select>
                      <input type="hidden" name="childHfTWNRoom2" value="0" id="childHfTWNRoom2">
                      <span class="age">W/Bed</span>
                    </td>
                    <td class="">
                      <select class="form-control selwtext" name="childWOBRoom2" id="childWOBRoom2">
                        <option value="0">0</option>
                        <option value="1">1</option>
                      </select>
                      <span class="age">WO/Bed</span>
                    </td>
                    <td class=""><span class="rtxt">Infant</span><br><span class="age">Below 2yrs</span></td>
                    <td class="">
                      <select class="form-control infantrooms" name="infantRoom2" id="infantRoom2">
                        <option value="0">0</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                      </select>
                    </td>
                    <td><a href="#" class="rem" data-id="2"><i class="fa fa-times fa-lg" aria-hidden="true"></i></a></td>
                  </tr> */}
                </table>
              </div>
              <div className="clearfix" />
              <br />
              <div className="form-group">
                <div className="col-md-12 smallsam" style={{fontSize: '11px'}}>
                  <input name="tc" id="checkbox-id" type="checkbox" />
                  <a href="#termdiv" data-toggle="collapse" className aria-expanded="true">I acknowledge to have read and accept the Terms and Conditions <i className="fa fa-angle-down" style={{fontSize: '14px'}} /></a>
                  <div id="termdiv" className="collapse in" aria-expanded="true" style={{}}>
                    <ul style={{listStyle: 'disc', marginLeft: '16px'}}>
                      <li>Prices shown are just guideline and are not guaranteed until purchase is completed.</li>
                      <li>Price shown are in SGD</li>
                      <li>Prices based on twin sharing, and does not include airport taxes and fuel surcharges, optional tours and gratitude for local guides, drivers and tour leaders, if applicable</li>
                      <li>All tours subject to group size for confirmation</li>
                      <li>All taxes quoted are subject to changes until the ticket(s) is issued</li>
                      <li>Land arrangement excludes flight arrangements, optional tours exclude gratitude for local guides, drivers and tour leaders, if applicable </li>
                      <li>Tour fare does not include visa charges. Passengers have to ensure that they hold a valid visa to enter their destination country. Please refer to <a href="http://cibtvisas.sg" target="_new">http://cibtvisas.sg</a> for visa requirement and charges</li>
                      <li>All passengers are categorised as adult, child or infant based on the tour's return date and not the date of booking</li>
                      <li>Please refer to <a href="https://www.euholidays.com.sg/tnc.html">https://www.euholidays.com.sg/tnc.html</a> to view our full set of terms and conditions</li>
                      <li>IMPORTANT NOTE: As a licensing condition of the Singapore Tourism Board, we EU HOLIDAYS PTE LTD are required to inform you, the Client, to consider purchasing travel insurance.</li>
                      <li>Get a comprehensive travel insurance policy to protect against unforeseen circumstances, such as baggage loss, flight delays, travel agent insolvency and medical emergencies.</li>
                    </ul>
                  </div>
                </div>
              </div>
              <div className="clearfix" />
              <div className="form-group btnrow">
                <div className="pull-right">
                  <a href="#" className="step3back">Back</a>
                  <Button id="CheckPriceNow" className="btn btn-sign" onClick={priceCheck} >CHECK PRICE AVAILABILITY</Button>				
                </div>
              </div>
            </form>

          </div>
          <div className="clearfix" />
          <div id="fstep2" style={{display: display2}}>
            <div className="row">
              <div className="col-md-1">
                <img src="https://www.euholidays.com.sg/assets/frontend/images/passport-icon.png" className="room-ico" align="left" />
              </div>
              <div className="col-md-4">
                <h3 className="sub-title">Main Contact Person</h3>
              </div>
              <div className="col-md-7">
                <hr className="customhr" />
              </div>
            </div>
            <div className="clearfix" />
            <div className="row" id="bookingstep2">
              <form className="main_contact_form">
                <div className="col-md-offset-2 col-md-10">
                  <div className="form-group">
                    <label className="col-md-1 control-label">Title</label>
                    <div className="col-md-3">
                      <select className="form-control" style={{marginTop: '-10px'}}>
                        <option value={1}>MR</option>
                        <option value={1}>MS</option>
                        <option value={2}>MRS</option>
                        <option value={3}>MISS</option>
                      </select>
                    </div>
                  </div>
                  <div className="clearfix" />
                  <div className="form-group">
                    <div className="col-md-6">
                      <input type="text" className="form-control" placeholder="*Given Name" />					 		
                    </div>
                    <div className="col-md-6">
                      <input type="text" className="form-control" placeholder="*Passport" />					 		
                    </div>
                  </div>
                  <div className="clearfix" />
                  <div className="form-group">
                    <div className="col-md-6">
                      <input type="text" className="form-control" placeholder="*Surname" />					 		
                    </div>
                    <div className="col-md-6">
                      <input type="text" className="form-control" placeholder="Address" />					 		
                    </div>
                  </div>
                  <div className="clearfix" />
                  <div className="form-group">
                    <div className="col-md-6">
                      <input type="text" className="form-control" placeholder="*Contact Number" />					 		
                    </div>
                    <div className="col-md-6">
                      <input type="text" className="form-control" placeholder="*Country" />					 		
                    </div>
                  </div>
                  <div className="clearfix" />
                  <div className="form-group">
                    <div className="col-md-6">
                      <input type="text" className="form-control" placeholder="*Email Address" />					 		
                    </div>
                    <div className="col-md-6">
                      <input type="text" className="form-control" placeholder="Postal Code" />					 		
                    </div>
                  </div>
                  <div className="clearfix" />
                  <div className="form-group">
                    <div className="col-md-12">
                      <div className="alert alert-info" style={{textAlign: 'center'}}>
                        Your Booking confirmation will be sent to this email address
                      </div>
                    </div>
                  </div>
                </div>
                <div className="clearfix" />
                <div className="row">
                  <div className="col-md-1">
                    <i className="fa fa-user fa-2x" />
                  </div>
                  <div className="col-md-4">
                    <h3 className="sub-title">Passenger Details</h3>
                  </div>
                  <div className="col-md-7">
                    <hr className="customhr" />
                  </div>
                </div>
                <div className="clearfix" />
                <div className="row">
                  <div className="col-md-12">
                    <div className="room-sub-title">
                      Room 1
                    </div>
                  </div>
                </div>
                <div className="clearfix" />
                <div className="row">
                  <div className="col-md-12">
                    <table className="bg-odd">
                      <tbody><tr>
                          <td className><span className="numb">1</span></td>
                          <td style={{padding: '10px'}}>Title:</td>
                          <td>
                            <select className="form-control title-f-small">
                              <option value={1}>MR</option>
                              <option value={1}>MS</option>
                              <option value={2}>MRS</option>
                              <option value={3}>MISS</option>
                            </select>
                          </td>
                          <td>
                            <input type="text" className="form-control inputbox thirdbox" placeholder="*Surname" />	
                          </td>
                          <td>
                            <input type="text" className="form-control inputbox" placeholder="*Given Name" />	
                          </td>
                          <td className="lastcol">
                            <div className="inner-addon right-addon">
                              <i className="glyphicon glyphicon-calendar" />
                              <input type="text" name="start_date" className="form-control inputbox" placeholder="Date Of Birth" />
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td colSpan={4} className="ptop10">
                            <select className="form-control">
                              <option value={1}>MR</option>
                              <option value={1}>MS</option>
                              <option value={2}>MRS</option>
                              <option value={3}>MISS</option>
                            </select>
                          </td>
                          <td>
                            <input type="text" className="form-control inputbox thirdbox" placeholder="NRIC" />	
                          </td>
                          <td className="lastcol">
                            <input type="text" className="form-control inputbox" placeholder="*Passport No" />	
                          </td>
                        </tr>
                        <tr>
                          <td colSpan={4}>
                            <div className="inner-addon right-addon">
                              <i className="glyphicon glyphicon-calendar" />
                              <input type="text" name="start_date" className="form-control" placeholder="Expiry Date" />
                            </div>
                          </td>
                          <td colSpan={4}>
                            <div className="inner-addon right-addon">
                              <i className="glyphicon glyphicon-calendar" />
                              <input type="text" name="start_date" className="form-control thirdbox w93" placeholder="Passport Issued Date" />
                            </div>
                          </td>
                        </tr>
                      </tbody></table>
                    <table className="bg-even">
                      <tbody><tr>
                          <td className><span className="numb">2</span></td>
                          <td style={{padding: '10px'}}>Title:</td>
                          <td>
                            <select className="form-control title-f-small">
                              <option value={1}>MR</option>
                              <option value={1}>MS</option>
                              <option value={2}>MRS</option>
                              <option value={3}>MISS</option>
                            </select>
                          </td>
                          <td>
                            <input type="text" className="form-control inputbox thirdbox" placeholder="*Surname" />	
                          </td>
                          <td>
                            <input type="text" className="form-control inputbox" placeholder="*Given Name" />	
                          </td>
                          <td className="lastcol">
                            <div className="inner-addon right-addon">
                              <i className="glyphicon glyphicon-calendar" />
                              <input type="text" name="start_date" className="form-control inputbox" placeholder="Date Of Birth" />
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td colSpan={4} className="ptop10">
                            <select className="form-control">
                              <option value={1}>MR</option>
                              <option value={1}>MS</option>
                              <option value={2}>MRS</option>
                              <option value={3}>MISS</option>
                            </select>
                          </td>
                          <td>
                            <input type="text" className="form-control inputbox thirdbox" placeholder="NRIC" />	
                          </td>
                          <td className="lastcol">
                            <input type="text" className="form-control inputbox" placeholder="*Passport No" />	
                          </td>
                        </tr>
                        <tr>
                          <td colSpan={4}>
                            <div className="inner-addon right-addon">
                              <i className="glyphicon glyphicon-calendar" />
                              <input type="text" name="start_date" className="form-control" placeholder="Expiry Date" />
                            </div>
                          </td>
                          <td colSpan={4}>
                            <div className="inner-addon right-addon">
                              <i className="glyphicon glyphicon-calendar" />
                              <input type="text" name="start_date" className="form-control thirdbox w93" placeholder="Passport Issued Date" />
                            </div>
                          </td>
                        </tr>
                      </tbody></table>
                    <div className="form-group btnrow">
                      <div className="pull-right">
                        <button type="button" className="btn btn-back-step" onClick={backtoStep1}>BACK</button>
                        <button type="button" className="btn btn-sign">CONFIRM BOOKING</button>
                      </div>
                    </div>
                  </div>
                  <div className="clearfix" />
                </div>
                {/*end step2*/}
              </form></div>
            {/* end left Panel */}				
          </div>

        </div>
        <div className="rsidebar is-affixed" id="rsidebar" style={{position: 'relative'}}>
        <div className="col-md-4 col-sm-12  col-xs-12 rsidebar__inner" id style={{position: 'relative', transform: 'translate3d(0px, 0px, 0px)'}}>			
          <div style={{background: '#fff'}}>	
            <div className="row bg-white">
              <div className="col-md-2" style={{marginLeft: '10px', marginTop: '5px'}}>
                <img src="https://www.euholidays.com.sg/assets/frontend/images/airplane.png" className="room-ico" align="left" />
              </div>
              <div className="col-md-5">
                <h3 className="sub-title">TOUR PRICE</h3>
              </div>
            </div>
            
            <br />
          </div>
          <div className="resize-sensor" style={{position: 'absolute', inset: '0px', overflow: 'hidden', zIndex: -1, visibility: 'hidden'}}><div className="resize-sensor-expand" style={{position: 'absolute', left: 0, top: 0, right: 0, bottom: 0, overflow: 'hidden', zIndex: -1, visibility: 'hidden'}}><div style={{position: 'absolute', left: '0px', top: '0px', transition: 'all 0s ease 0s', width: '100000px', height: '100000px'}} /></div><div className="resize-sensor-shrink" style={{position: 'absolute', left: 0, top: 0, right: 0, bottom: 0, overflow: 'hidden', zIndex: -1, visibility: 'hidden'}}><div style={{position: 'absolute', left: 0, top: 0, transition: '0s', width: '200%', height: '200%'}} /></div></div></div>
          {priceCheckTotal}
        </div>
      </div>
    </>

  return <div style={{ margin: "32px" }}>
    {bookingJsx}
  </div>
}

export default OutBoundPage
